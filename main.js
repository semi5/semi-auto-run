export function setup(ctx) {
    const id = 'semi-auto-run';
    const title = 'SEMI Auto Run';

    const adjustedMaxHit = (player, enemy) => {
        let maxhit = enemy.getAttackMaxDamage(enemy.nextAttack);
        maxhit = enemy.applyDamageModifiers(player, maxhit);
        maxhit += enemy.modifiers.getFlatReflectDamage();
        return Math.ceil(maxhit);
    };

    const runFromCombat = (reason) => {
        game.combat.stop();
        notifyPlayer(game.attack, `[${title}] Ran from combat, ${reason}.`, 'danger');
        mod.api.SEMI.log(id, `Ran from combat, ${reason}.`);
    };

    const isAutoEatInstalled = () => {
        return (mod.manager.getLoadedModList().indexOf("SEMI Auto Eat") !== -1);
    }

    ctx.patch(Enemy, 'queueNextAction').after((result) => {
        const player = game.combat.player;
        const enemy = game.combat.enemy;
        const currentFood = player.food.currentSlot;
        const currentFoodQty = currentFood.quantity;
        const currentFoodHealing = player.getFoodHealing(currentFood.item);

        const attackDamage = adjustedMaxHit(player, enemy);

        // The next hit lower then current hitpoints
        if (attackDamage < player.hitpoints) {
            return;
        }

        // Max Hit
        if (attackDamage >= player.stats.maxHitpoints) {
            runFromCombat('next hit is greater then max hitpoints');
            return;
        }

        // No SEMI Auto Eat Mod | No food
        if (currentFoodQty <= 0 || !isAutoEatInstalled()) {
            runFromCombat('next hit is higher then current hitpoints, and SEMI Auto Eat is not installed');
            return;
        }

        // SEMI Auto Eat Mod
        if ((currentFoodQty * currentFoodHealing + player.hitpoints) <= attackDamage) {
            runFromCombat('next hit cannot be healed with remaining food stores');
            return;
        }

    });

    mod.api.SEMI.log(id, 'Successfully loaded!');
}